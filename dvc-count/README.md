# dvc-count

premier exemple dvc

dvc stage list
count  Outputs lines
titi   Outputs calcul
dvc-count (master *=) $ d

dvc-count (master *%=) $ dvc dag
+-------+
| count |
+-------+
    *
    *
    *
+------+
| titi |
+------+

dvc repro
Running stage 'count':
> python hello.py
écriture du fichier lines
Generating lock file 'dvc.lock'
Updating lock file 'dvc.lock'

Running stage 'titi':
> python titi.py
écriture du fichier `calcul`
Updating lock file 'dvc.lock'

To track the changes with git, run:

	git add dvc.lock

To enable auto staging, run:

	dvc config core.autostage true
Use `dvc push` to send your updates to remote storage.
dvc-count (master *%=) $


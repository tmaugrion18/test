"""simple line counter
"""
lines = 0
with open('test.txt', 'r') as fh:
    lines = len(fh.readlines())

# print(lines)

with open('lines', 'w') as fh:
    fh.write(str(lines))

print("écriture du fichier lines")


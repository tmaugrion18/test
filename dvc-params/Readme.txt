Testeur de paramètres
======================

**prérequis** : git et dvc installés

lancer la commande : dvc repro


dvc repro
Running stage 'learn':
> python deep.py
Generating lock file 'dvc.lock'
Updating lock file 'dvc.lock'

To track the changes with git, run:

	git add dvc.lock

To enable auto staging, run:

	dvc config core.autostage true
Use `dvc push` to send your updates to remote storage.


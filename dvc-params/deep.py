"""This script stands for showing the retreived parameters
"""

import yaml
import pprint

with open("params.yaml", 'r') as fd:
    params = yaml.safe_load(fd)

epoch = params['train']['epoch']
learning_rate = params['train']['learning_rate']
batch_size = params['train']['batch_size']
threshold = params['process']['threshold']

# ... do some deep learning stuff with these parameters
# here we just print the parameters in an outpout in a file, that's all

params = {"epoch": epoch, "learning_rate": learning_rate, "batch_size": batch_size, "threshold": threshold}

with open('data/output.txt', 'w') as fhandle:
    fhandle.write("parameters arguments of the script are: \n")

    ppp = pprint.PrettyPrinter(indent=4, stream=fhandle)
    ppp.pprint(params)

